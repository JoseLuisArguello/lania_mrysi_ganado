/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 15-ac128la
 */
public class calcularPersonasTiempoParaArreglosTest {
    
    public calcularPersonasTiempoParaArreglosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcularPersonasTiempoParaArreglos method, of class calcularPersonasTiempoParaArreglos.
     */
    @Test
    public void testCalcularPersonasTiempoParaArreglos() {
        System.out.println("Mostrar información de las variables ");
        int n = 0;
        calcularPersonasTiempoParaArreglos instance = new calcularPersonasTiempoParaArreglos();
        int[] expResult = null;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
       // assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
        System.out.println(expResult);
        System.out.println(result);
        System.out.println(n);
        System.out.println("-------------------------");
    }
    
    /*1*/
    @Test
    public void testObtenertiempo() {
        System.out.println("Punto Uno - Tiempo de preparación de cada arreglos");
        System.out.println("Tiempo que tarda en arreglar un florero");
        int n = 12;
        String min = "";
        calcularPersonasTiempoParaArreglos instance = new calcularPersonasTiempoParaArreglos();
        
        int totaldedatos=2;
        int[] expResult = new int[totaldedatos]; 
        for(int i=0; i<totaldedatos; i++){
		expResult[i]=i;
                expResult[i]=i;
        }
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        int resultado = result[0];
        if(resultado <= 1){
             min = " Minutos ";
         }
         else{
             min = " Horas ";
         }  
        System.out.println("Resultado:");
        System.out.println("Operación 1 :"+ n+ "x" + 15 +"="+ n*15);
        System.out.println("Operación 2 :"+ n*15 + "/" + 60 +"="+result[0]);
        System.out.println(result[0]+ min);
        System.out.println("-------------------------");
    }
    
    /*2*/
    @Test
    public void testcuantaspersonasrealizanunarreglo() {
        System.out.println("Punto Cuatro");
        System.out.println("Cuantas personas se necesitan para hacer un arreglo");   
        int n = 126;
        String min = "";
        calcularPersonasTiempoParaArreglos instance = new calcularPersonasTiempoParaArreglos();
        
        int totaldedatos=2;
        int[] expResult = new int[totaldedatos]; 
        for(int i=0; i<totaldedatos; i++){
		expResult[i]=i;
                expResult[i]=i;
        }
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        
        
        System.out.println("Resultado:");
        System.out.println("Total de Arreglos :"+ n);
        System.out.println("Total de Personal realizando los arreglos :"+ result[1]);        
        System.out.println("-------------------------");
    }
    
     /*3*/
    @Test
    public void testCalcular_condicionesdearreglos() {
    System.out.println("Punto dos, tres y cinco - Obtener tiempo y total de personas");
        System.out.println("Si se piden de 1 a 19 arreglos, se asigna una persona a ese encargo");
        System.out.println("Si se piden de 20 a 49 arreglos, se asignan dos personas a ese encargo");
        System.out.println("Si se piden de 50 arreglos en adelante, se asigna una persona por cada 25 arreglos y una persona extra. ");
        System.out.println("Por ejemplo 50 arreglos o 60 arreglos serían atendidos por un equipo de tres personas pero 75 arreglos se harían entre 4 personas");
        int n = 51;
        String min = "";
        calcularPersonasTiempoParaArreglos instance = new calcularPersonasTiempoParaArreglos();
        
        int totaldedatos=2;
        int[] expResult = new int[totaldedatos]; 
        for(int i=0; i<totaldedatos; i++){
		expResult[i]=i;
                expResult[i]=i;
        }
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        int resultado = result[0];
        if(resultado <= 1){
             min = " Minutos ";
         }
         else{
             min = " Horas ";
         }  
        
        System.out.println("Resultado:");
        System.out.println("Total de Arreglos :"+ n);
        System.out.println("Tiempo tardarán en terminar la tarea :"+ result[1]+ " " + min);
        System.out.println("Total de Personal realizando los arreglos :"+ result[1]);        
        System.out.println("-------------------------");
    }
    
     /*4*/
    @Test
    public void testCalcularPersonasTiempoParaArreglosde_exepcion() {
        String text = "";
        System.out.println("Si rebasa de 101 arreglos");
        System.out.println("Resultado:");
        int n = 150;
        String min = "";
        calcularPersonasTiempoParaArreglos instance = new calcularPersonasTiempoParaArreglos();
        
        int totaldedatos=2;
        int[] expResult = new int[totaldedatos]; 
        for(int i=0; i<totaldedatos; i++){
		expResult[i]=i;
                expResult[i]=i;
        }
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        int resultado = result[0];
        if(resultado <= 1){
             min = " Minutos ";
         }
         else{
             min = " Horas ";
         }  
        if(n <= 100){
           text="Su pedido sera procesado a la brevedad";
        }
        else{
           text="Total de Arreglos rebaso el nivel de pedido";
        }
        System.out.println(text);  
        System.out.println("Tiempo tardarán en terminar los arreglos :"+ result[0] + " " + min);
        System.out.println("-------------------------");
    }
    
      /*5*/
    @Test
    public void testCalcularPersonasValoresnegativos() {
        String text = "";
        System.out.println("Validación para valores negativos ");
        System.out.println("Resultado:");
        int n = -1;
        String min = "";
        calcularPersonasTiempoParaArreglos instance = new calcularPersonasTiempoParaArreglos();
            
        if(n < 0){
            n = (-n) * (-n); 
            text="No se permiten valores negativos";
        }
        else{
            text="Valores positivos";
        }
        
        int totaldedatos=2;
        int[] expResult = new int[totaldedatos]; 
        for(int i=0; i<totaldedatos; i++){
		expResult[i]=i;
                expResult[i]=i;
        }
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        
        System.out.println(text); 
        System.out.println("Total de Arreglos :"+ n);
        System.out.println("-------------------------");
    }
    
}
