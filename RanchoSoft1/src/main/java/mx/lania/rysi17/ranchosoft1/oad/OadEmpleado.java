/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft1.oad;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.List;
import mx.lania.rysi17.ranchosoft1.entidades.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author 15-ac128la
 */
public interface OadEmpleado extends JpaRepository<Empleado, Integer>{
    
    /*Obtener la lista del nombre*/
    List<Empleado> findByNombreContaining(String cadena);    
    
    /*Obtener los nombres con autocomplete*/
    List<Empleado> findByNombreStartingWithIgnoreCase(String cadena);
    
    
    
}
