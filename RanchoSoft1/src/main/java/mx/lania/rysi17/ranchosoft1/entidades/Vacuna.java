/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft1.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 15-ac128la
 */
@Entity
@Table(name = "vacunas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vacuna.findAll", query = "SELECT v FROM Vacuna v")
    , @NamedQuery(name = "Vacuna.findByIdVacunas", query = "SELECT v FROM Vacuna v WHERE v.idVacunas = :idVacunas")
    , @NamedQuery(name = "Vacuna.findByIdProveedor", query = "SELECT v FROM Vacuna v WHERE v.idProveedor = :idProveedor")})
public class Vacuna implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdVacunas")
    private Integer idVacunas;
    @Lob
    @Size(max = 65535)
    @Column(name = "NombreVacuna")
    private String nombreVacuna;
    @Column(name = "IdProveedor")
    private Integer idProveedor;

    public Vacuna() {
    }

    public Vacuna(Integer idVacunas) {
        this.idVacunas = idVacunas;
    }

    public Integer getIdVacunas() {
        return idVacunas;
    }

    public void setIdVacunas(Integer idVacunas) {
        this.idVacunas = idVacunas;
    }

    public String getNombreVacuna() {
        return nombreVacuna;
    }

    public void setNombreVacuna(String nombreVacuna) {
        this.nombreVacuna = nombreVacuna;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVacunas != null ? idVacunas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vacuna)) {
            return false;
        }
        Vacuna other = (Vacuna) object;
        if ((this.idVacunas == null && other.idVacunas != null) || (this.idVacunas != null && !this.idVacunas.equals(other.idVacunas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.rysi17.ranchosoft1.entidades.Vacuna[ idVacunas=" + idVacunas + " ]";
    }
    
}
