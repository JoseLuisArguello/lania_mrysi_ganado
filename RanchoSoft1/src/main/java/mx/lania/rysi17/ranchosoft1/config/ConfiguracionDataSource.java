/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft1.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

/**
 *
 * @author 15-ac128la
 */
@Configuration
public class ConfiguracionDataSource {
    
    @Bean(destroyMethod = "close")
    @Profile({"desarrollo", "produccion"})
    public DataSource dataSource() {
        try {
            Properties hcProps = PropertiesLoaderUtils.loadProperties(new ClassPathResource("/db.properties"));
            HikariConfig hc = new HikariConfig(hcProps);
            return new HikariDataSource(hc);

        } catch (IOException ex) {
            Logger.getLogger(ConfiguracionDataSource.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /*@Bean
    @Profile("pruebas")
    public DataSource getMockDataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.DERBY)
                .addScript("db/sicoba.sql")
                .addScript("db/pruebas-datos.sql")
                .build();
    } */    
    
}
