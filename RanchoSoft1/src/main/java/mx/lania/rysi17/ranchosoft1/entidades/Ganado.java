/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft1.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 15-ac128la
 */
@Entity
@Table(name = "ganado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ganado.findAll", query = "SELECT g FROM Ganado g")
    , @NamedQuery(name = "Ganado.findByIdGanado", query = "SELECT g FROM Ganado g WHERE g.idGanado = :idGanado")
    , @NamedQuery(name = "Ganado.findByIdProveedores", query = "SELECT g FROM Ganado g WHERE g.idProveedores = :idProveedores")
    , @NamedQuery(name = "Ganado.findByPrecioGanado", query = "SELECT g FROM Ganado g WHERE g.precioGanado = :precioGanado")
    , @NamedQuery(name = "Ganado.findByTipoGanado", query = "SELECT g FROM Ganado g WHERE g.tipoGanado = :tipoGanado")
    , @NamedQuery(name = "Ganado.findByIdProveedor", query = "SELECT g FROM Ganado g WHERE g.idProveedor = :idProveedor")})
public class Ganado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdGanado")
    private Integer idGanado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idProveedores")
    private int idProveedores;
    @Lob
    @Size(max = 65535)
    @Column(name = "DescripcionGanado")
    private String descripcionGanado;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PrecioGanado")
    private BigDecimal precioGanado;
    @Column(name = "TipoGanado")
    private Short tipoGanado;
    @Column(name = "IdProveedor")
    private Integer idProveedor;

    public Ganado() {
    }

    public Ganado(Integer idGanado) {
        this.idGanado = idGanado;
    }

    public Ganado(Integer idGanado, int idProveedores) {
        this.idGanado = idGanado;
        this.idProveedores = idProveedores;
    }

    public Integer getIdGanado() {
        return idGanado;
    }

    public void setIdGanado(Integer idGanado) {
        this.idGanado = idGanado;
    }

    public int getIdProveedores() {
        return idProveedores;
    }

    public void setIdProveedores(int idProveedores) {
        this.idProveedores = idProveedores;
    }

    public String getDescripcionGanado() {
        return descripcionGanado;
    }

    public void setDescripcionGanado(String descripcionGanado) {
        this.descripcionGanado = descripcionGanado;
    }

    public BigDecimal getPrecioGanado() {
        return precioGanado;
    }

    public void setPrecioGanado(BigDecimal precioGanado) {
        this.precioGanado = precioGanado;
    }

    public Short getTipoGanado() {
        return tipoGanado;
    }

    public void setTipoGanado(Short tipoGanado) {
        this.tipoGanado = tipoGanado;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGanado != null ? idGanado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ganado)) {
            return false;
        }
        Ganado other = (Ganado) object;
        if ((this.idGanado == null && other.idGanado != null) || (this.idGanado != null && !this.idGanado.equals(other.idGanado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.rysi17.ranchosoft1.entidades.Ganado[ idGanado=" + idGanado + " ]";
    }
    
}
