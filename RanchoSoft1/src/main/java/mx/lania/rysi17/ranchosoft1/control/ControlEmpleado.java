/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft1.control;

import java.util.List;
import mx.lania.rysi17.ranchosoft1.entidades.Empleado;
import mx.lania.rysi17.ranchosoft1.oad.OadEmpleado;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author 15-ac128la
 */

@RestController
@RequestMapping("/empleados")
public class ControlEmpleado {
    
     private static Logger Logger = LoggerFactory.getLogger(ControlEmpleado.class);
    
    @Autowired
    OadEmpleado OadEmpleado;
    
    
     /**mostrar los datos por defaul GET*/
    @RequestMapping("")
    public List<Empleado> getEmpleado(){
        Logger.debug("GET / Empleados");
        return OadEmpleado.findAll();
    }
    
    
   /* @RequestMapping("")
    public List<Empleado> getEmpleado(){
         return OadEmpleado.findAll();
    }
     */
    
    
     /**Regresa un valor mediente un id*/
     @RequestMapping("/{id}")
     public Empleado getEmpleadoporId(@PathVariable("id") Integer id){
         return OadEmpleado.findOne(id);
     }
     
      /*BUSQUEDA*/
    @RequestMapping(value="", params = {"usuario"}, method=RequestMethod.GET)
    @ResponseBody
    public List<Empleado> buscarEmpleados(@RequestParam("usuario") String cadena) {
        return OadEmpleado.findByNombreContaining(cadena);
    }
    
}
