/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft1.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 15-ac128la
 */
@Entity
@Table(name = "proveedores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proveedore.findAll", query = "SELECT p FROM Proveedore p")
    , @NamedQuery(name = "Proveedore.findByIdProveedores", query = "SELECT p FROM Proveedore p WHERE p.idProveedores = :idProveedores")
    , @NamedQuery(name = "Proveedore.findByTelProveedor", query = "SELECT p FROM Proveedore p WHERE p.telProveedor = :telProveedor")
    , @NamedQuery(name = "Proveedore.findByCpProveedor", query = "SELECT p FROM Proveedore p WHERE p.cpProveedor = :cpProveedor")})
public class Proveedore implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idProveedores")
    private Integer idProveedores;
    @Lob
    @Size(max = 65535)
    @Column(name = "NombreProveedor")
    private String nombreProveedor;
    @Lob
    @Size(max = 65535)
    @Column(name = "DireccionProveedor")
    private String direccionProveedor;
    @Size(max = 30)
    @Column(name = "TelProveedor")
    private String telProveedor;
    @Size(max = 5)
    @Column(name = "CpProveedor")
    private String cpProveedor;

    public Proveedore() {
    }

    public Proveedore(Integer idProveedores) {
        this.idProveedores = idProveedores;
    }

    public Integer getIdProveedores() {
        return idProveedores;
    }

    public void setIdProveedores(Integer idProveedores) {
        this.idProveedores = idProveedores;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public String getDireccionProveedor() {
        return direccionProveedor;
    }

    public void setDireccionProveedor(String direccionProveedor) {
        this.direccionProveedor = direccionProveedor;
    }

    public String getTelProveedor() {
        return telProveedor;
    }

    public void setTelProveedor(String telProveedor) {
        this.telProveedor = telProveedor;
    }

    public String getCpProveedor() {
        return cpProveedor;
    }

    public void setCpProveedor(String cpProveedor) {
        this.cpProveedor = cpProveedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProveedores != null ? idProveedores.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proveedore)) {
            return false;
        }
        Proveedore other = (Proveedore) object;
        if ((this.idProveedores == null && other.idProveedores != null) || (this.idProveedores != null && !this.idProveedores.equals(other.idProveedores))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.rysi17.ranchosoft1.entidades.Proveedore[ idProveedores=" + idProveedores + " ]";
    }
    
}
