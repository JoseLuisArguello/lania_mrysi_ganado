/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft1.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 15-ac128la
 */
@Entity
@Table(name = "detallevacuna")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detalle_vacuna.findAll", query = "SELECT d FROM Detalle_vacuna d")
    , @NamedQuery(name = "Detalle_vacuna.findByIdDetalleVacuna", query = "SELECT d FROM Detalle_vacuna d WHERE d.idDetalleVacuna = :idDetalleVacuna")
    , @NamedQuery(name = "Detalle_vacuna.findByIdEmpleados", query = "SELECT d FROM Detalle_vacuna d WHERE d.idEmpleados = :idEmpleados")
    , @NamedQuery(name = "Detalle_vacuna.findByIdVacunas", query = "SELECT d FROM Detalle_vacuna d WHERE d.idVacunas = :idVacunas")
    , @NamedQuery(name = "Detalle_vacuna.findByIdGanado", query = "SELECT d FROM Detalle_vacuna d WHERE d.idGanado = :idGanado")
    , @NamedQuery(name = "Detalle_vacuna.findByIdVacuna", query = "SELECT d FROM Detalle_vacuna d WHERE d.idVacuna = :idVacuna")
    , @NamedQuery(name = "Detalle_vacuna.findByFechaVacuna", query = "SELECT d FROM Detalle_vacuna d WHERE d.fechaVacuna = :fechaVacuna")})
public class Detalle_vacuna implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdDetalleVacuna")
    private Integer idDetalleVacuna;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdEmpleados")
    private int idEmpleados;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdVacunas")
    private int idVacunas;
    @Column(name = "IdGanado")
    private Integer idGanado;
    @Column(name = "IdVacuna")
    private Integer idVacuna;
    @Column(name = "FechaVacuna")
    @Temporal(TemporalType.DATE)
    private Date fechaVacuna;

    public Detalle_vacuna() {
    }

    public Detalle_vacuna(Integer idDetalleVacuna) {
        this.idDetalleVacuna = idDetalleVacuna;
    }

    public Detalle_vacuna(Integer idDetalleVacuna, int idEmpleados, int idVacunas) {
        this.idDetalleVacuna = idDetalleVacuna;
        this.idEmpleados = idEmpleados;
        this.idVacunas = idVacunas;
    }

    public Integer getIdDetalleVacuna() {
        return idDetalleVacuna;
    }

    public void setIdDetalleVacuna(Integer idDetalleVacuna) {
        this.idDetalleVacuna = idDetalleVacuna;
    }

    public int getIdEmpleados() {
        return idEmpleados;
    }

    public void setIdEmpleados(int idEmpleados) {
        this.idEmpleados = idEmpleados;
    }

    public int getIdVacunas() {
        return idVacunas;
    }

    public void setIdVacunas(int idVacunas) {
        this.idVacunas = idVacunas;
    }

    public Integer getIdGanado() {
        return idGanado;
    }

    public void setIdGanado(Integer idGanado) {
        this.idGanado = idGanado;
    }

    public Integer getIdVacuna() {
        return idVacuna;
    }

    public void setIdVacuna(Integer idVacuna) {
        this.idVacuna = idVacuna;
    }

    public Date getFechaVacuna() {
        return fechaVacuna;
    }

    public void setFechaVacuna(Date fechaVacuna) {
        this.fechaVacuna = fechaVacuna;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetalleVacuna != null ? idDetalleVacuna.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detalle_vacuna)) {
            return false;
        }
        Detalle_vacuna other = (Detalle_vacuna) object;
        if ((this.idDetalleVacuna == null && other.idDetalleVacuna != null) || (this.idDetalleVacuna != null && !this.idDetalleVacuna.equals(other.idDetalleVacuna))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.rysi17.ranchosoft1.entidades.Detalle_vacuna[ idDetalleVacuna=" + idDetalleVacuna + " ]";
    }
    
}
