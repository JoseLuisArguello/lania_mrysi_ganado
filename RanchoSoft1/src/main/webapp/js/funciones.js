/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var base_url = "http://localhost:8084/RanchoSoft1/";


var error = base_url + "img/alerta_error.png";
var bien = base_url + "img/alerta_bien.png";
var alerta = base_url + "img/alerta_alerta.png";
var imagen_error = '<img src="' + error + '" height="40px" width="40px">';
var imagen_bien = '<img src="' + bien + '" height="45px" width="45px">';
var imagen_alerta = '<img src="' + alerta + '" height="40px" width="40px">';

/*--------------------VALIDACIONES CON .validate()----------------------------*/
function validaciones() {
    $.validator.setDefaults({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    //Personalización de mensajes de error en español
    jQuery.extend(jQuery.validator.messages, {
        required: "Este campo es obligatoro.",
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Por favor, introduzca una fecha válida.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Porfavor ingresa solo numeros.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Por favor, introduzca la misma contraseña.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Porfavor ingresa un folio mayor a {0}.")
    });
}

/*****************************************************************************************/
/*#MENU PRINCIPAL*/
function btnRedir(e) {
	//alert(e);
    if (e === 1) {
       location.href = "operaciones/index.php";	  
    } else if (e === 2) {		
        location.href = "01_empleados/index.html";
    } else if (e === 3) {
		location.href = "reportes/index.php";
    }else if (e === 4) {
		location.href = "herramientas/index.php";
    }else if (e === 5) {
		location.href = "historial/index.php";
    }else if (e === 15) {
		location.href = "personal/index.php";
    }else if (e === 17) {
		location.href = "vehiculos/index.php";
    } 
	
	
}