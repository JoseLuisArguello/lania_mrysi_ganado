<%-- 
    Document   : index
    Created on : 16/08/2017, 08:03:23 PM
    Author     : 15-ac128la
--%>

 <%@include file="../plantillas/header.jsp" %>
 
  <div class="container">
      <h3>EMPLEADOS</h3>
      
     
      <section id="ap-empleado">
            <header><h1>Clientes</h1></header>
                        <nav>
        <ul>
            <li> <a href="index.html" name="Inicio">Inicio</a> </li>
        </ul>
    </nav>
            
             <section>
                <div id="notificacion"></div>
                <div id="forma">
                    <table>
                        <tr><td>Nombre:</td><td><input type="text" id="inp-nombre"></td></tr>
                        <tr><td>Login</td><td><input type="text" id="inp-login"></td></tr>
                        <tr><td>email</td><td><input type="text" id="inp-email"></td></tr>
                        <tr><td>Fecha</td><td><input type="text" id="inp-fechaRegistro"></td></tr>
                        <tr><td></td><td><button id='bt-guardar' class='bt-guardar'>Guardar</button>
                                <input type="hidden" id="inp-idCliente"></td></tr>
                        
                    </table>
                </div>
                <div id="busqueda">
                    <input type="text" id="inp-busqueda">
                    <button id="bt-buscar">Buscar</button>
                </div>
            </section>
                       
            
            <section>
               <!--  <ul id="lista-articulos"></ul> -->
                <table id="lista-empleado"></table>
            </section>
        </section>
      
      
      
      
      
      
        <!-- PLANTILLAS -->
        <script type='text/template' id='plantilla-empleado'>
            <td class="art-NombreEmpleados"><%= empelado.getMatricula() %> nombreEmpleados </td>
            <td class='art-Usuario'> usuario </td>
            <td class='art-Password_2'> password2 </td>
            <td class='art-DireccionEmpleado'> direccionEmpleado </td>
            <td class='art-TelEmpleado'> telEmpleado</td>         
            </td>            
            
            <td class='art-boton'>
                    <button id='bt-editar-idEmpleados ' class='bt-editar'>E</button>
            </td>
            <td class='art-boton'>
                    <button id='bt-borrar-idEmpleados ' class='bt-borrar'>X</button>
            </td>
        </script>
           <!-- DEPENDENCIAS JAVASCRIPT -->
        <script src="../js/jquery.min.js"></script>
        <script src="../js/underscore-min.js"></script>
        <script src="../js/backbone-min.js"></script>
        <script type="text/javascript">
                 var apArts = {};
        
                apArts.Empleado = Backbone.Model.extend({
                defaults:{ nombre: "empleados"},
                idAttribute : 'idEmpleados',
                urlRoot:'/RanchoSoft1/Empleado',
                initialize: function(){
                    console.log('Inicializacion Empleado');
                        this.on('change',function() {
                             console.log('Cambio de Empleado');
                        });
                },
                validate: function(atribs){
                    console.log('Validando Empleado');                        
                    }
                });
                
                apArts.ListaEmpleado = Backbone.Collection.extend({
                    model: apArts.Empleado,
                    url:'/RanchoSoft1/Empleado'
                });
                
                 apArts.EmpeladoEnPantalla = new apArts.ListaEmpleado();
                 
                  apArts.VistaEmpleado = Backbone.View.extend({
                   // tagName: 'li',
                    tagName: 'tr',
                    template: _.template($('#plantilla-empleado').html()),  
                    render: function(){
                        //this.$el.html(this.model.attributes.nombre);                        
                         this.$el.html(this.template(this.model.toJSON()));  
                         return this;
                    }
                });
                 
                /*Asociar la vista o lista*/
                apArts.VistaListaEmpleado = Backbone.View.extend({
                    el: "#ap-empleado",
                events:{
                    'click .bt-borrar':'borrarEmpleado',
                    'click #bt-guardar':'guardarEmpleado',
                    'click .bt-editar':'editarEmplado',
                    'click #bt-buscar':'buscarEmpleado'
                }, 
                initialize: function(){
                        console.log('Iniciliza lista');
                        apArts.EmpleadoEnPantalla.on('add', this.addOne, this);
                        apArts.EmpleadoEnPantalla.on('reset', this.addAll, this);
                        apArts.EmpleadoEnPantalla.fetch();                        
                    },
                    addOne:function(art){
                        console.log('Lista addOne');
                        var vista = new apArts.VistaEmpleado({model: art});
                        $('#lista-empleado').append(vista.render().el);                        
                    },
                    addAll:function(){
                        console.log('Lista addAll');
                        this.$( "#lista-empleado").html('');  //cadena vacion indica que borra todo lo que tengas
                        apArts.EmpleadoEnPantalla.each(this.addOne, this);   // agregando todo on each                       
                    },
                    buscarCliente: function(ev){
                          console.log('buscar');                                            
                           apArts.EmpleadoEnPantalla.fetch({data: $.param({nombre: $('#inp-busqueda').val()}), reset:true});
                           $('#inp-busqueda').val('');
                          // var art1 = apArts.articulosEnPantalla.fetch(idArt);
                    }
                   
              
                });
                
                
                apArts.vistaArts = new apArts.VistVistaListaEmpleadoaListaEmpleado();
        </script>   
            
        
        
        
        
        
        
      
   <!--    
  <div class="row">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s6">
          <i class="material-icons prefix">account_circle</i>
          <input id="icon_prefix" type="text" class="validate">
          <label for="icon_prefix">First Name</label>
        </div>
        <div class="input-field col s6">
          <i class="material-icons prefix">phone</i>
          <input id="icon_telephone" type="tel" class="validate">
          <label for="icon_telephone">Telephone</label>
        </div>
      </div>
    </form>
  </div>
  </div>
 
 
 
 
 
 
 
        
        
        <section id="ap-cliente">
            <header><h1>Clientes</h1></header>
                        <nav>
        <ul>
            <li> <a href="index.html" name="Inicio">Inicio</a> </li>
        </ul>
    </nav>
            
             <section>
                <div id="notificacion"></div>
                <div id="forma">
                    <table>
                        <tr><td>Nombre:</td><td><input type="text" id="inp-nombre"></td></tr>
                        <tr><td>Login</td><td><input type="text" id="inp-login"></td></tr>
                        <tr><td>email</td><td><input type="text" id="inp-email"></td></tr>
                        <tr><td>Fecha</td><td><input type="text" id="inp-fechaRegistro"></td></tr>
                        <tr><td></td><td><button id='bt-guardar' class='bt-guardar'>Guardar</button>
                                <input type="hidden" id="inp-idCliente"></td></tr>
                        
                    </table>
                </div>
                <div id="busqueda">
                    <input type="text" id="inp-busqueda">
                    <button id="bt-buscar">Buscar</button>
                </div>
            </section>
                       
            
            <section>
              
                <table id="lista-cliente"></table>
            </section>
        </section>
 
       
     -->
   

    
<%@include file="../plantillas/footer.jsp" %>
