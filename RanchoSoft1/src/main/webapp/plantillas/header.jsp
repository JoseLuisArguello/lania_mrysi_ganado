<%-- 
    Document   : encabezado_menu
    Created on : 9/11/2016, 10:55:06 PM
    Author     : 15-ac128la
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="<%=request.getContextPath()+"/css/materialize.css"%>">
        <link rel="stylesheet" href="<%=request.getContextPath()+"/css/materialize.min.css"%>">
        <link rel="stylesheet" href="<%=request.getContextPath()+"/css/style.css"%>">
       
        <title>SICOBA - Sistema de Control Bacuno</title>
   <style>
    /*    ROJO = #a80033;
    VERDE= #44783c;*/
    .contenedor_menu{
        padding: 1em;

    }
    .contenedor_menu div.btnAnimacion{

        display:block;
        text-align:center;
        height:205px;
        width:190px;
        text-decoration: none;
        overflow: hidden;
        position: relative;
        float: left;        	
    }

    .btnAnimacion div{
        cursor: pointer;

    }
    .contenedor_menu div.btnAnimacion:hover{
        background:rgba(0, 0, 0, 0.1);
    }
    .contenedor_menu div h2{
        font-size:16px;
        font-weight:bold;
        position: relative; 
    }
    .contenedor_menu div h3{
        font-size:12px;
        position: relative;
    }

    .contenedor_menu div a{
        font-size:12px;
        position: relative;
    }

    .contenedor_menu div img {
        position: relative;
        border: 0;
        width: 90px;
        height: 90px;
    }

    .contenedor_menu div img.gris {
        filter: url('#grayscale'); /* Versión SVG para navegadores modernos */
        -ms-filter: url('#grayscale');
        -webkit-filter: url('#grayscale');
        filter: grayscale(100%);
        -webkit-filter: grayscale(100%);
        -moz-filter: grayscale(100%);
        -ms-filter: grayscale(100%);
        -o-filter: grayscale(100%);
        filter: grayscale(100%);
        filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale"); /* Firefox 10+, Firefox on Android */
        filter: gray; /* IE6-9 */
        -webkit-filter: grayscale(100%); 
        filter: Gray(); /* IE4-9 */

    }

    .rojo {
        color: rgb(196, 0, 48);
    }

    .verde {
        color: rgb(0, 121, 52);
    }
    .gris {
        color: gray;
    }


</style>
 
    </head>
    <body>
        
     <nav class="cyan" style="background-color: #ffffff">
            <div class="nav-wrapper">
              <a href="#!" class="brand-logo"><i class="material-icons"><img src="<%=request.getContextPath()+"/img/02_LogoPrincipal.png"%>" width="50" height="50"></i>SICOBA</a>
              <ul class="right hide-on-med-and-down">
                <li><a href="sass.html"><i class="material-icons">search</i></a></li>
                <li><a href="badges.html"><i class="material-icons">view_module</i></a></li>
                <li><a href="collapsible.html"><i class="material-icons">refresh</i></a></li>
                <li><a href="mobile.html"><i class="material-icons">more_vert</i></a></li>
              </ul>
            </div>
        </nav>  
        
     <!--  <nav>
           <div class="nav-wrapper" style="background-color: #ffffff">
            <center><a href="#" class="brand-logo"><font style='color:black'><img src="<%=request.getContextPath()+"/img/02_LogoPrincipal.png"%>" width="50" height="50">SICOBA</font></a></center>
        </div>
      </nav>-->
   
