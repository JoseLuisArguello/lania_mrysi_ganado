CREATE TABLE Alimentos (
  IdAlimentos INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  DescripcionAlimentos TEXT NULL,
  PRIMARY KEY(IdAlimentos)
);

CREATE TABLE Clientes (
  IdClientes INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  NombreCliente TEXT NULL,
  DireccionCliente TEXT NULL,
  TelCliente VARCHAR(50) NULL,
  RfcCliente VARCHAR(20) NULL,
  PRIMARY KEY(IdClientes)
);

CREATE TABLE DetalleAlimentos (
  IdDetalleAlimentos INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  IdEmpleados INTEGER UNSIGNED NOT NULL,
  IdGanado INTEGER UNSIGNED NULL,
  FechaAlimentos DATE NULL,
  IdAlimentos INTEGER UNSIGNED NULL,
  IdEmpleado INTEGER UNSIGNED NULL,
  PRIMARY KEY(IdDetalleAlimentos),
  INDEX DetalleAlimentos_FKIndex1(IdGanado),
  INDEX DetalleAlimentos_FKIndex2(IdAlimentos),
  INDEX DetalleAlimentos_FKIndex3(IdEmpleados)
);

CREATE TABLE DetalleAseo (
  IdDetalleAseo INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  IdEmpleados INTEGER UNSIGNED NOT NULL,
  IdGanado INTEGER UNSIGNED NULL,
  IdEmpleado INTEGER UNSIGNED NULL,
  FechaAseo DATE NULL,
  HoraAseo TIME NULL,
  PRIMARY KEY(IdDetalleAseo),
  INDEX DetalleAseo_FKIndex1(IdGanado),
  INDEX DetalleAseo_FKIndex2(IdEmpleados)
);

CREATE TABLE DetalleVacuna (
  IdDetalleVacuna INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  IdEmpleados INTEGER UNSIGNED NOT NULL,
  IdVacunas INTEGER UNSIGNED NOT NULL,
  IdGanado INTEGER UNSIGNED NULL,
  IdVacuna INTEGER UNSIGNED NULL,
  FechaVacuna DATE NULL,
  PRIMARY KEY(IdDetalleVacuna),
  INDEX DetalleVacuna_FKIndex1(IdGanado),
  INDEX DetalleVacuna_FKIndex2(IdVacunas),
  INDEX DetalleVacuna_FKIndex3(IdEmpleados)
);

CREATE TABLE DetalleVenta (
  IdDetalleVenta INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  IdVenta INTEGER UNSIGNED NULL,
  Subtotal DECIMAL(10,2) NULL,
  IdGanado INTEGER UNSIGNED NULL,
  PRIMARY KEY(IdDetalleVenta),
  INDEX DetalleVenta_FKIndex1(IdVenta),
  INDEX DetalleVenta_FKIndex2(IdGanado)
);

CREATE TABLE Empleados (
  IdEmpleados INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  NombreEmpleados TEXT NULL,
  Usuario VARCHAR(50) NULL,
  Password_2 VARCHAR(50) NULL,
  DireccionEmpleado TEXT NULL,
  TelEmpleado VARCHAR(50) NULL,
  PRIMARY KEY(IdEmpleados)
);

CREATE TABLE GANADO (
  IdGanado INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  idProveedores INTEGER UNSIGNED NOT NULL,
  DescripcionGanado TEXT NULL,
  PrecioGanado DECIMAL(10,2) NULL,
  TipoGanado TINYINT(3) UNSIGNED NULL,
  IdProveedor INTEGER UNSIGNED NULL,
  PRIMARY KEY(IdGanado),
  INDEX GANADO_FKIndex1(idProveedores)
);

CREATE TABLE Proveedores (
  idProveedores INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  NombreProveedor TEXT NULL,
  DireccionProveedor TEXT NULL,
  TelProveedor VARCHAR(30) NULL,
  CpProveedor VARCHAR(5) NULL,
  PRIMARY KEY(idProveedores)
);

CREATE TABLE Vacunas (
  IdVacunas INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  NombreVacuna TEXT NULL,
  IdProveedor INTEGER UNSIGNED NULL,
  PRIMARY KEY(IdVacunas)
);

CREATE TABLE VENTA (
  IdVenta INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  IdClientes INTEGER UNSIGNED NOT NULL,
  TotalVenta DECIMAL(10,2) NULL,
  FechaVenta DATE NULL,
  HoraVenta TIME NULL,
  IdEmpleados INTEGER UNSIGNED NULL,
  PRIMARY KEY(IdVenta),
  INDEX VENTA_FKIndex1(IdClientes),
  INDEX VENTA_FKIndex2(IdEmpleados)
);


