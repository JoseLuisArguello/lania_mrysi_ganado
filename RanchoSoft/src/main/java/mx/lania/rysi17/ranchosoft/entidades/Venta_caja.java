/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 15-ac128la
 */
@Entity
@Table(name = "venta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Venta_caja.findAll", query = "SELECT v FROM Venta_caja v")
    , @NamedQuery(name = "Venta_caja.findByIdVenta", query = "SELECT v FROM Venta_caja v WHERE v.idVenta = :idVenta")
    , @NamedQuery(name = "Venta_caja.findByIdClientes", query = "SELECT v FROM Venta_caja v WHERE v.idClientes = :idClientes")
    , @NamedQuery(name = "Venta_caja.findByTotalVenta", query = "SELECT v FROM Venta_caja v WHERE v.totalVenta = :totalVenta")
    , @NamedQuery(name = "Venta_caja.findByFechaVenta", query = "SELECT v FROM Venta_caja v WHERE v.fechaVenta = :fechaVenta")
    , @NamedQuery(name = "Venta_caja.findByHoraVenta", query = "SELECT v FROM Venta_caja v WHERE v.horaVenta = :horaVenta")
    , @NamedQuery(name = "Venta_caja.findByIdEmpleados", query = "SELECT v FROM Venta_caja v WHERE v.idEmpleados = :idEmpleados")})
public class Venta_caja implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdVenta")
    private Integer idVenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdClientes")
    private int idClientes;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TotalVenta")
    private BigDecimal totalVenta;
    @Column(name = "FechaVenta")
    @Temporal(TemporalType.DATE)
    private Date fechaVenta;
    @Column(name = "HoraVenta")
    @Temporal(TemporalType.TIME)
    private Date horaVenta;
    @Column(name = "IdEmpleados")
    private Integer idEmpleados;

    public Venta_caja() {
    }

    public Venta_caja(Integer idVenta) {
        this.idVenta = idVenta;
    }

    public Venta_caja(Integer idVenta, int idClientes) {
        this.idVenta = idVenta;
        this.idClientes = idClientes;
    }

    public Integer getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Integer idVenta) {
        this.idVenta = idVenta;
    }

    public int getIdClientes() {
        return idClientes;
    }

    public void setIdClientes(int idClientes) {
        this.idClientes = idClientes;
    }

    public BigDecimal getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(BigDecimal totalVenta) {
        this.totalVenta = totalVenta;
    }

    public Date getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(Date fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public Date getHoraVenta() {
        return horaVenta;
    }

    public void setHoraVenta(Date horaVenta) {
        this.horaVenta = horaVenta;
    }

    public Integer getIdEmpleados() {
        return idEmpleados;
    }

    public void setIdEmpleados(Integer idEmpleados) {
        this.idEmpleados = idEmpleados;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVenta != null ? idVenta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Venta_caja)) {
            return false;
        }
        Venta_caja other = (Venta_caja) object;
        if ((this.idVenta == null && other.idVenta != null) || (this.idVenta != null && !this.idVenta.equals(other.idVenta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.rysi17.ranchosoft.entidades.Venta_caja[ idVenta=" + idVenta + " ]";
    }
    
}
