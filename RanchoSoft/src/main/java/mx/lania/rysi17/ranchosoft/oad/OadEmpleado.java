/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft.oad;

/**
 *
 * @author 15-ac128la
 */
import java.util.List;
import mx.lania.rysi17.ranchosoft.entidades.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OadEmpleado extends JpaRepository<Empleado, Integer>{
    
    List<Empleado> findByNombreContaining(String cadena);
    
}
