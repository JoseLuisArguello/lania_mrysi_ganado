/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 15-ac128la
 */
@Entity
@Table(name = "alimentos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Alimento.findAll", query = "SELECT a FROM Alimento a")
    , @NamedQuery(name = "Alimento.findByIdAlimentos", query = "SELECT a FROM Alimento a WHERE a.idAlimentos = :idAlimentos")})
public class Alimento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdAlimentos")
    private Integer idAlimentos;
    @Lob
    @Size(max = 65535)
    @Column(name = "DescripcionAlimentos")
    private String descripcionAlimentos;

    public Alimento() {
    }

    public Alimento(Integer idAlimentos) {
        this.idAlimentos = idAlimentos;
    }

    public Integer getIdAlimentos() {
        return idAlimentos;
    }

    public void setIdAlimentos(Integer idAlimentos) {
        this.idAlimentos = idAlimentos;
    }

    public String getDescripcionAlimentos() {
        return descripcionAlimentos;
    }

    public void setDescripcionAlimentos(String descripcionAlimentos) {
        this.descripcionAlimentos = descripcionAlimentos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAlimentos != null ? idAlimentos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alimento)) {
            return false;
        }
        Alimento other = (Alimento) object;
        if ((this.idAlimentos == null && other.idAlimentos != null) || (this.idAlimentos != null && !this.idAlimentos.equals(other.idAlimentos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.rysi17.ranchosoft.entidades.Alimento[ idAlimentos=" + idAlimentos + " ]";
    }
    
}
