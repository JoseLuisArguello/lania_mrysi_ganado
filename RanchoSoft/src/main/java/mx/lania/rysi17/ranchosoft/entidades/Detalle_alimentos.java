/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 15-ac128la
 */
@Entity
@Table(name = "detallealimentos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detalle_alimentos.findAll", query = "SELECT d FROM Detalle_alimentos d")
    , @NamedQuery(name = "Detalle_alimentos.findByIdDetalleAlimentos", query = "SELECT d FROM Detalle_alimentos d WHERE d.idDetalleAlimentos = :idDetalleAlimentos")
    , @NamedQuery(name = "Detalle_alimentos.findByIdEmpleados", query = "SELECT d FROM Detalle_alimentos d WHERE d.idEmpleados = :idEmpleados")
    , @NamedQuery(name = "Detalle_alimentos.findByIdGanado", query = "SELECT d FROM Detalle_alimentos d WHERE d.idGanado = :idGanado")
    , @NamedQuery(name = "Detalle_alimentos.findByFechaAlimentos", query = "SELECT d FROM Detalle_alimentos d WHERE d.fechaAlimentos = :fechaAlimentos")
    , @NamedQuery(name = "Detalle_alimentos.findByIdAlimentos", query = "SELECT d FROM Detalle_alimentos d WHERE d.idAlimentos = :idAlimentos")
    , @NamedQuery(name = "Detalle_alimentos.findByIdEmpleado", query = "SELECT d FROM Detalle_alimentos d WHERE d.idEmpleado = :idEmpleado")})
public class Detalle_alimentos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdDetalleAlimentos")
    private Integer idDetalleAlimentos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdEmpleados")
    private int idEmpleados;
    @Column(name = "IdGanado")
    private Integer idGanado;
    @Column(name = "FechaAlimentos")
    @Temporal(TemporalType.DATE)
    private Date fechaAlimentos;
    @Column(name = "IdAlimentos")
    private Integer idAlimentos;
    @Column(name = "IdEmpleado")
    private Integer idEmpleado;

    public Detalle_alimentos() {
    }

    public Detalle_alimentos(Integer idDetalleAlimentos) {
        this.idDetalleAlimentos = idDetalleAlimentos;
    }

    public Detalle_alimentos(Integer idDetalleAlimentos, int idEmpleados) {
        this.idDetalleAlimentos = idDetalleAlimentos;
        this.idEmpleados = idEmpleados;
    }

    public Integer getIdDetalleAlimentos() {
        return idDetalleAlimentos;
    }

    public void setIdDetalleAlimentos(Integer idDetalleAlimentos) {
        this.idDetalleAlimentos = idDetalleAlimentos;
    }

    public int getIdEmpleados() {
        return idEmpleados;
    }

    public void setIdEmpleados(int idEmpleados) {
        this.idEmpleados = idEmpleados;
    }

    public Integer getIdGanado() {
        return idGanado;
    }

    public void setIdGanado(Integer idGanado) {
        this.idGanado = idGanado;
    }

    public Date getFechaAlimentos() {
        return fechaAlimentos;
    }

    public void setFechaAlimentos(Date fechaAlimentos) {
        this.fechaAlimentos = fechaAlimentos;
    }

    public Integer getIdAlimentos() {
        return idAlimentos;
    }

    public void setIdAlimentos(Integer idAlimentos) {
        this.idAlimentos = idAlimentos;
    }

    public Integer getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(Integer idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetalleAlimentos != null ? idDetalleAlimentos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detalle_alimentos)) {
            return false;
        }
        Detalle_alimentos other = (Detalle_alimentos) object;
        if ((this.idDetalleAlimentos == null && other.idDetalleAlimentos != null) || (this.idDetalleAlimentos != null && !this.idDetalleAlimentos.equals(other.idDetalleAlimentos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.rysi17.ranchosoft.entidades.Detalle_alimentos[ idDetalleAlimentos=" + idDetalleAlimentos + " ]";
    }
    
}
