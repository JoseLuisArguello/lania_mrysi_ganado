/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 15-ac128la
 */
@Entity
@Table(name = "detalleaseo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detalle_aseo.findAll", query = "SELECT d FROM Detalle_aseo d")
    , @NamedQuery(name = "Detalle_aseo.findByIdDetalleAseo", query = "SELECT d FROM Detalle_aseo d WHERE d.idDetalleAseo = :idDetalleAseo")
    , @NamedQuery(name = "Detalle_aseo.findByIdEmpleados", query = "SELECT d FROM Detalle_aseo d WHERE d.idEmpleados = :idEmpleados")
    , @NamedQuery(name = "Detalle_aseo.findByIdGanado", query = "SELECT d FROM Detalle_aseo d WHERE d.idGanado = :idGanado")
    , @NamedQuery(name = "Detalle_aseo.findByIdEmpleado", query = "SELECT d FROM Detalle_aseo d WHERE d.idEmpleado = :idEmpleado")
    , @NamedQuery(name = "Detalle_aseo.findByFechaAseo", query = "SELECT d FROM Detalle_aseo d WHERE d.fechaAseo = :fechaAseo")
    , @NamedQuery(name = "Detalle_aseo.findByHoraAseo", query = "SELECT d FROM Detalle_aseo d WHERE d.horaAseo = :horaAseo")})
public class Detalle_aseo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdDetalleAseo")
    private Integer idDetalleAseo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdEmpleados")
    private int idEmpleados;
    @Column(name = "IdGanado")
    private Integer idGanado;
    @Column(name = "IdEmpleado")
    private Integer idEmpleado;
    @Column(name = "FechaAseo")
    @Temporal(TemporalType.DATE)
    private Date fechaAseo;
    @Column(name = "HoraAseo")
    @Temporal(TemporalType.TIME)
    private Date horaAseo;

    public Detalle_aseo() {
    }

    public Detalle_aseo(Integer idDetalleAseo) {
        this.idDetalleAseo = idDetalleAseo;
    }

    public Detalle_aseo(Integer idDetalleAseo, int idEmpleados) {
        this.idDetalleAseo = idDetalleAseo;
        this.idEmpleados = idEmpleados;
    }

    public Integer getIdDetalleAseo() {
        return idDetalleAseo;
    }

    public void setIdDetalleAseo(Integer idDetalleAseo) {
        this.idDetalleAseo = idDetalleAseo;
    }

    public int getIdEmpleados() {
        return idEmpleados;
    }

    public void setIdEmpleados(int idEmpleados) {
        this.idEmpleados = idEmpleados;
    }

    public Integer getIdGanado() {
        return idGanado;
    }

    public void setIdGanado(Integer idGanado) {
        this.idGanado = idGanado;
    }

    public Integer getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(Integer idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public Date getFechaAseo() {
        return fechaAseo;
    }

    public void setFechaAseo(Date fechaAseo) {
        this.fechaAseo = fechaAseo;
    }

    public Date getHoraAseo() {
        return horaAseo;
    }

    public void setHoraAseo(Date horaAseo) {
        this.horaAseo = horaAseo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetalleAseo != null ? idDetalleAseo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detalle_aseo)) {
            return false;
        }
        Detalle_aseo other = (Detalle_aseo) object;
        if ((this.idDetalleAseo == null && other.idDetalleAseo != null) || (this.idDetalleAseo != null && !this.idDetalleAseo.equals(other.idDetalleAseo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.rysi17.ranchosoft.entidades.Detalle_aseo[ idDetalleAseo=" + idDetalleAseo + " ]";
    }
    
}
