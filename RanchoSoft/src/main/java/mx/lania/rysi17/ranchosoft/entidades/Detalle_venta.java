/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.lania.rysi17.ranchosoft.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 15-ac128la
 */
@Entity
@Table(name = "detalleventa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detalle_venta.findAll", query = "SELECT d FROM Detalle_venta d")
    , @NamedQuery(name = "Detalle_venta.findByIdDetalleVenta", query = "SELECT d FROM Detalle_venta d WHERE d.idDetalleVenta = :idDetalleVenta")
    , @NamedQuery(name = "Detalle_venta.findByIdVenta", query = "SELECT d FROM Detalle_venta d WHERE d.idVenta = :idVenta")
    , @NamedQuery(name = "Detalle_venta.findBySubtotal", query = "SELECT d FROM Detalle_venta d WHERE d.subtotal = :subtotal")
    , @NamedQuery(name = "Detalle_venta.findByIdGanado", query = "SELECT d FROM Detalle_venta d WHERE d.idGanado = :idGanado")})
public class Detalle_venta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdDetalleVenta")
    private Integer idDetalleVenta;
    @Column(name = "IdVenta")
    private Integer idVenta;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Subtotal")
    private BigDecimal subtotal;
    @Column(name = "IdGanado")
    private Integer idGanado;

    public Detalle_venta() {
    }

    public Detalle_venta(Integer idDetalleVenta) {
        this.idDetalleVenta = idDetalleVenta;
    }

    public Integer getIdDetalleVenta() {
        return idDetalleVenta;
    }

    public void setIdDetalleVenta(Integer idDetalleVenta) {
        this.idDetalleVenta = idDetalleVenta;
    }

    public Integer getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Integer idVenta) {
        this.idVenta = idVenta;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public Integer getIdGanado() {
        return idGanado;
    }

    public void setIdGanado(Integer idGanado) {
        this.idGanado = idGanado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetalleVenta != null ? idDetalleVenta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detalle_venta)) {
            return false;
        }
        Detalle_venta other = (Detalle_venta) object;
        if ((this.idDetalleVenta == null && other.idDetalleVenta != null) || (this.idDetalleVenta != null && !this.idDetalleVenta.equals(other.idDetalleVenta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.lania.rysi17.ranchosoft.entidades.Detalle_venta[ idDetalleVenta=" + idDetalleVenta + " ]";
    }
    
}
